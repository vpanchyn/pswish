// TODO: add handling of store.playstation.com tabs opened to trigger update

const STORE_TAB_CHECK_INTERVAL = 500;
const STORE_TAB_CLOSE_DELAY = 5000;
const TICK_INTERVAL = 1000;

const TAB_COMPLETE_STATUS = 'complete';

const UPDATE_LABEL = 'upd';
const LOGIN_LABEL = 'login';
const NA_LABEL = 'NA';

const BADGE_COLORS = {
  INFO: '#bbb',
  ERROR: '#f00',
  SUCCESS: '#f0ad4e'
}

const GA_EVENTS = {
  USER_WISHLISS_LOADED: 'userWishListsLoaded',
  LOGIN_ERROR: 'connectionError',
  DATA_UPDATE_STARTED: 'dataUpdateStarted',
  ALL_GAMES_LOADED: 'allGamesDataLoaded'
}

const STORE_URL = 'https://store.playstation.com';
const USER_WISHLISTS_URL = 'https://store.playstation.com/kamaji/api/valkyrie_storefront/00_09_000/gateway/lists/v1/users/me/lists?listTypes=WISHLIST&limit=10';
const WISHLIST_URL = (listId) => `https://store.playstation.com/kamaji/api/valkyrie_storefront/00_09_000/gateway/lists/v1/users/me/lists/${listId}/items?limit=50&offset=0`;
const GAME_DATA_URL = (id) => `https://store.playstation.com/valkyrie-api/en/US/19/resolve/${id}`;

var gamesList = {};
var cantLogin = false;
var loaded = false;

let listId;
let gameIds;
let currentGame = 0;
let storeTabId;
let tabInterval;
let isInFailureState = false;

function getUserWishLists() {
  fetch(USER_WISHLISTS_URL)
    .then(rawData => rawData.json())
    .then(json => {
      sendEvent(GA_EVENTS.USER_WISHLISS_LOADED);
      console.log('...got list of wish lists');
      listId = json.lists[0].listId;
      isInFailureState = false;
      getGamesList();
    })
    .catch(handleError);
}

function handleError(data) {
    sendEvent(GA_EVENTS.LOGIN_ERROR);
    updateBadge(LOGIN_LABEL, BADGE_COLORS.ERROR);
    if (!isInFailureState) {
      chrome.tabs.create({
        url: STORE_URL,
        active: false
      }, handleTab);
      isInFailureState = true;
    } else {
      cantLogin = true;
    }
    loaded = true;
}

function handleTab(tab) {
  storeTabId = tab.id;
  tabInterval = setInterval(() => {
    chrome.tabs.get(storeTabId, checkTabStatus);
  }, STORE_TAB_CHECK_INTERVAL);
}

function checkTabStatus(tab) {
  updateBadge(UPDATE_LABEL, BADGE_COLORS.INFO);
  if (tab.status === TAB_COMPLETE_STATUS) {
    clearInterval(tabInterval);
    setTimeout(() => {
      chrome.tabs.remove(storeTabId);
      getUserWishLists();
    }, STORE_TAB_CLOSE_DELAY);
  }
}

function getGamesList() {
  sendEvent(GA_EVENTS.DATA_UPDATE_STARTED)
  updateBadge(UPDATE_LABEL, BADGE_COLORS.INFO);
  loaded = false;
  cantLogin = false;
  if (!listId) {
    getUserWishLists();
    return;
  }

  fetch(WISHLIST_URL(listId))
    .then(rawData => rawData.json())
    .then(json => {
      console.log('...got game list');
      gameIds = json.items.map(item => {
        return item.itemId;
      });
      currentGame = 0;
      getGameData();
    })
    .catch(handleError);
}

function updateBadge(text, color) {
  chrome.browserAction.setBadgeBackgroundColor({color: color});
  chrome.browserAction.setBadgeText({text: text});
}

function loadNext() {
  currentGame++;
  if (currentGame < gameIds.length) {
    getGameData();
  } else {
    sendEvent(GA_EVENTS.ALL_GAMES_LOADED);
    console.log('...got all games');
    updateBadge(_.filter(gamesList, 'discount').length.toString(), BADGE_COLORS.SUCCESS);
    loaded = true;
  }
}

function getGameData() {
  fetch(GAME_DATA_URL(gameIds[currentGame]))
    .then(rawData => rawData.json())
    .then(json => {
      updateGameData(gameIds[currentGame], json);
      loadNext();
    })
    .catch(error => {
      loadNext();
    })
}

function convertGameData(gameData, gameId) {
  let realPrice = NA_LABEL;
  let displayPrice = NA_LABEL;

  const mainData = gameData.included[0].attributes;
  const defSKU = mainData.skus[0].prices;
  const userPirce = defSKU['non-plus-user'];
  const actualPrice = userPirce['actual-price'];

  if (defSKU) { //game is released!
    realPrice = actualPrice.value;
    displayPrice = actualPrice.display;
  }

  const parsedData = {
    id: gameId,
    name: mainData.name,
    price: realPrice,
    displayPrice: displayPrice
  }

  if (userPirce['discount-percentage']) {
    parsedData.discount = {
      amount: userPirce['discount-percentage'],
      price: realPrice,
      displayPrice: displayPrice,
      plusOnly: userPirce['is-plus'],
      endDate: userPirce.availability['end-date']
    };
  }

  return parsedData;
}

function updateGameData(gameId, gameData) {
  gamesList[gameId] = convertGameData(gameData, gameId);
}

function tick()
{
    var mins = new Date().getMinutes();
    if(mins === '00'){
        getGamesList();
     }
}

function init() {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-106139624-1', 'auto');
  ga('set', 'checkProtocolTask', function() { });
  sendEvent('init');

  setInterval(tick, TICK_INTERVAL);
  getGamesList();
}

function sendEvent(eventAction) {
  ga('send', 'event', {
    eventAction,
    eventCategory: 'bg'
  });
}

window.addEventListener('load', init);
