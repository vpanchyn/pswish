const STATE = {
  LOGIN_ERROR: 0,
  DEFAULT: 1,
  INFO: 2
}

const INTERVAL_DELAY = 500;

var app = new Vue({
  el: '#app',
  data: {
    bg: null,
    chrome,
    refreshInterval: null,
    searchString: '',
    filteredGamesList: null,
    gamesList: null,
    loading: false,
    state: 1,
    STATE,
    INTERVAL_DELAY
  },
  created: function () {
    this.chrome.runtime.getBackgroundPage(bgObj => {
      this.bg = bgObj;
      this.sendEvent('init')
      this.redrawGamesList();

      if (this.bg.cantLogin) {
        this.state = this.STATE.LOGIN_ERROR;
      } else {
        this.state = this.STATE.DEFAULT;
      }
    });
  },
  methods: {
    reload: function () {
      this.sendEvent('refreshingGameList')
      this.bg.getGamesList();
      this.gamesList = [];
      this.loading = true;
      this.refreshInterval = setInterval(() => {
        if (this.bg.loaded) {
          this.state = this.STATE.DEFAULT;
          this.redrawGamesList();
          this.loading = false;
          clearInterval(refreshInterval);
        }
        }, INTERVAL_DELAY);
    },
    filterGamesList: function () {
      this.filteredGamesList = _.filter(this.gamesList, game => {
        return _.includes(game.name.toLowerCase(), this.searchString.toLowerCase());
      })
    },
    redrawGamesList: function () {
      this.gamesList = _.chain(this.bg.gamesList)
        .map()
        .orderBy(['discount','price'])
        .value();
      this.filterGamesList();
    },
    openGame: function (game) {
      const normalizedName = this.normalize(game.name);
      this.sendEvent('gameOpened', game.name)
      window.open(`https://store.playstation.com/#!/en-us/games/${normalizedName}/cid=${game.id}`);
    },
    toggleInfo: function () {
      this.state = this.state === this.STATE.DEFAULT ? this.STATE.INFO : this.STATE.DEFAULT;
    },
    openTwitter: function () {
      window.open('https://twitter.com/b2kdaman');
    },
    openStore: function () {
      window.open('https://store.playstation.com');
    },
    normalize: function (name) {
      return name.replace(/[™®:]+/g, '').replace(/[ /]/g,'-').toLowerCase();
    },
    sendEvent: function (eventAction, eventValue = null) {
      this.bg.ga('send', 'event', {
        eventAction,
        eventCategory: 'popup',
        eventValue
      });
    }
  },
  watch: {
    searchString: function () {
      this.sendEvent('searchUpdated', this.searchString)
      this.redrawGamesList();
    }
  }
});
